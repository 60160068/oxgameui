package com.mycompany.oxgameui;

import java.io.Serializable;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 *
 * @author Windows10
 */
public class Player implements Serializable {
    private char name;
    private int win;
    private int lose;
    private int draw;
    
    public char getName() {
        return name;
    }
    public int getWin() {
        return win;
    }
    public void win(){
        win++;
    }
    public int getLose() {
        return lose;
    }    
     public void lose(){
        lose++;
    }
    public int getDraw() {
        return draw;
    }
     public void draw(){
        draw++;
    }
    public Player(char name){
        this.name = name;
    }

    @Override
    public String toString() {
        return "Player{" + "name=" + name + ", win=" + win + ", lose=" + lose + ", draw=" + draw + '}';
    }
    
}
